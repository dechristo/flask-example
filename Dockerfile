FROM alpine:3.4

RUN apk update
RUN apk add python3-dev make gcc
RUN apk add --no-cache python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache

ADD . /app
WORKDIR /app

RUN pip install -r requirements.txt
RUN export FLASK_APP=src/router.py
CMD ["python3 -m flask run", "router.py"]