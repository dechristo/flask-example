
.PHONY: tests run
tests:
	pytest --cov=src tests -v
run:
	python3 -m flask run
