# A Flask example API for handling login creation

## 1. Introduction
This small app was built with Python 3, Flask and MongoDB. It's containerized with Docker and uses Docker Compose to ties the application with MongoDB.
It also uses virtualenv in order to separate the app dependecies in its own contained environment.
**Flask** was chosen due to its versatility, implicity and easy to integrate.

## 1.1 Instructions
After cloning or downloading at the root folder type: `virtualenv -p -python3 env` in order to create a virtual environment. To activate it: `source env/bin/activate`.

To install its dependencies type: `pip3 install -r requirements.txt`.

Build the containers with: `docker-compose build` and then run with `docker-compose up`


## 2. Endpoints
### 2.1 POST /api/login
`curl -X POST -F "email=luke@rebelalliance.com" -F "password=1234" localhost:5000/api/login`

In case of success the following message will be displayed:

	"User dudu.christo@rebelalliance.com logged in!"
	

If you not provide any of the parameters you will receive a **500** and:

	{
  	"Error": "something went wrong! Lets check the logs..."
	}


### 2.2 POST /api/account 
`curl -X POST -F "email=dudu.christo@gmail.com" -F "password=1234" localhost:5000/api/account`

Response: 

	"Your account has been created! Please, activate your account on the following link:http://localhost:5000/api/activate/account"
	
In case no username and/or password is given the message will be:

	"Sorry, cannot create an account without user and password!"
### 2.3 PATCH /api/account
`curl -X PATCH -F "email=dudu.christo@gmail.com" localhost:5000/api/account`

	"Userdudu.christo@gmail.com has been successfully activated!"
### 2.4 GET /api/public/yodathewise
As this is a public endpoint, and no need to login, any user should receive a **200** and the message:

	"Try not. Do or do not. There's no try."

### 2.5 GET /api/protected/snoke
`curl localhost:5000/api/protected/snoke`
If you are not logged in to the system, the response will be a **401** with

	Unauthorized Access
	
Then, once you loggged:
`curl -u dudu.christo@gmail.com:1234 localhost:5000/api/protected/snoke` with a **200** and the message:

	"Light! To balance the darkness..."
### 2.5 GET /api/protected/vader
If you are not logged in to the system, the response will be a **401** with

	Unauthorized Access
	
Then, once you loggged:
`curl -u dudu.christo@gmail.com:1234 localhost:5000/api/protected/vader` with a **200** and the message:

	"Your lack of faith is disturbing!"
## 3. Running






dudu@dudu-HP-Pacurl -X POST -F "email=dudu.christo@gmail.com" -F "password=1234" localhost:5000/api/login
"User dudu.christo@gmail.com logged in!"




## 4. Testing
Run the command `make tests`. The output should looks like:

	----------- coverage: platform linux, python 3.5.2-final-0 -----------
	Name                                  Stmts   Miss  Cover
	---------------------------------------------------------
	src/__init__.py                           0      0   100%
	src/controllers/__init__.py               0      0   100%
	src/controllers/login_controller.py      20      1    95%
	src/controllers/user_controller.py       21      2    90%
	src/models/__init__.py                    0      0   100%
	src/models/login.py                      11      0   100%
	src/models/user.py                       21      0   100%
	src/router.py                            38     38     0%
	src/services/__init__.py                  0      0   100%
	src/services/login_service.py             8      3    62%
	src/services/mongo_service.py            47     23    51%
	src/services/user_service.py             14      0   100%
	src/utils/__init__.py                     0      0   100%
	src/utils/crypto.py                       6      0   100%
	---------------------------------------------------------
	TOTAL                                   186     67    64%



## 5. Running locally
If you wish to run the application locally, without containers, first export the **FLASK_APP** environment variable: `export FLASK_APP=src/router.py`

Then type `make run`

*Don't forget to activate the virtual environment first!

