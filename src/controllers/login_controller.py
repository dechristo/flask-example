from src.services.login_service import LoginService
from src.models.login import Login


class LoginController:

    def __init__(self, params):
        self.params = params
        self.login_service = LoginService()

    def do_login(self):
        if not self.__is_valid():
            return None

        self.login_info = Login(self.params)

        login = self.login_service.login(self.login_info)

        if not login:
            return None

        return "User {0} logged in!".format(self.login_info.email)


    def __is_valid(self):
        if not 'email' in self.params or not 'password' in self.params:
            return False

        if not self.params['email'] or not self.params['password']:
            return False

        return True