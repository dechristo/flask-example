from src.services.user_service import UserService
from src.models.user import User


class UserController:

    def __init__(self):
        self._user_service = UserService()


    def create_account(self, params):
        email = params.get('email')
        password = params.get('password')

        if not email or not password:
            return 'Sorry, cannot create an account without user and password!'

        user = User(email, password)
        result = self._user_service.save(user)

        if result:
            return 'Your account has been created! Please, activate your account on the following link:' \
                   'http://localhost:5000/api/activate/account'

        return 'Something went wrong, call reinforcements. Lets go take a look at the logs...'


    def activate_account(self, params):
        email = params.get('email')
        result = self._user_service.activate(email)

        if result:
            return 'User {0} has been successfully activated!'.format(email)

        return 'Uh oh! Something went wrong with the activation.'
