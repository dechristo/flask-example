from src.utils.crypto import Crypto


class Login:

    def __init__(self, params):
        self.__email = params['email']
        self.__password = Crypto.encrypt(params['password'])


    @property
    def email(self):
        return self.__email


    @email.setter
    def email(self, email):
        self.__email = email


    @property
    def password(self):
        return self.__password
