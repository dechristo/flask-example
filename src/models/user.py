from src.utils.crypto import Crypto


class User:

    def __init__(self, email, password, is_logged_in=False, is_active=False):
        self.__email = email
        self.__password = Crypto.encrypt(password)
        self.__is_logged_in = is_logged_in
        self.__is_active = is_active


    @property
    def email(self):
        return self.__email


    @email.setter
    def email(self, email):
        self.__email = email


    @property
    def password(self):
        return self.__password


    @property
    def is_logged_in(self):
        return self.__is_logged_in


    @is_logged_in.setter
    def is_logged_in(self, value):
        self.__is_logged_in = value


    @property
    def is_active(self):
        return self.__is_active


    @is_active.setter
    def is_active(self, value):
        self.__is_active = value
