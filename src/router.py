from flask import Flask, request, jsonify
from src.controllers.user_controller import UserController
from src.controllers.login_controller import LoginController
from flask_httpauth import HTTPBasicAuth


APP = Flask(__name__)
APP.secret_key = 'notasafeone'
USER_CONTROLLER = UserController()
auth = HTTPBasicAuth()

@auth.verify_password
def verify_pw(email, password):
    login_data = {
        'email' : email,
        'password' : password
    }

    login_controller = LoginController(login_data)
    content = login_controller.do_login()

    if content:
        return True

    return False


@APP.route("/api/login", methods=['POST'])
def login():
    login_controller = LoginController(request.form)
    content = login_controller.do_login()

    return __sendResponse(content)


@APP.route("/api/account", methods=['POST'])
def create_account():
    content = USER_CONTROLLER.create_account(request.form)
    return __sendResponse(content)


@APP.route("/api/account", methods=['PATCH'])
def verify_account():
    content = USER_CONTROLLER.activate_account(request.form)
    return __sendResponse(content)


@APP.route("/api/public/yodathewise", methods=['GET'])
def yoda_the_wise():
    return __sendResponse("Try not. Do or do not. There's no try.")


@APP.route("/api/protected/snoke", methods=['GET'])
@auth.login_required
def snoke():
    return __sendResponse('Light! To balance the darkness...')


@APP.route("/api/protected/vader", methods=['GET'])
@auth.login_required
def vader():
    return __sendResponse('Your lack of faith is disturbing!')


def __sendResponse(content):
    if content:
        return jsonify(content), 200

    return jsonify({"Error": "something went wrong! Lets check the logs..."}), 500


APP.run(host='0.0.0.0', port=5000)
