from src.services.mongo_service import MongoService


class LoginService:

    def __init__(self):
        self.db_conn = MongoService()

    def login(self, login_info):
        filter = {
        '$and': [
            {'email' : login_info.email},
            {'password': login_info.password}
        ]}
        user = self.db_conn.find('users', filter)

        return user