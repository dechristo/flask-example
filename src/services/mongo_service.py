from pymongo import MongoClient, errors

from src.settings import Settings


class MongoService():

    def __init__(self):
        self.__url = Settings.DB_URL
        self.__client = MongoClient(self.__url)
        self.__database = None


    def save(self, col, object):
        if not col:
            raise Exception('collection cannot be empty!')

        if not object:
            raise Exception('Trying to insert a null object!')

        if not self.is_connected(): self.connect(self.__url)

        try:
            self.__database[col].insert_one(object)
            return True
        except (AttributeError, errors.OperationFailure) as err:
            print('Error saving to database: {0}'.format(err))


    def update(self, col, property, new_value, filter, filter_value):
        if not col:
            raise Exception('collection cannot be empty!')

        if not property:
            raise Exception('property cannot be empty!')

        if not filter:
            raise Exception('filter cannot be empty!')

        if not self.is_connected(): self.connect(self.__url)

        try:
            self.__database[col].update_one({
                filter: filter_value
                },{'$set': {
                    property: new_value
                    }
                }, upsert=False)
            return True

        except (AttributeError, errors.OperationFailure) as err:
            print('Error activating user {0}: {1}'.format(err, filter_value))


    def find(self, col, filter):
        if not col:
            raise Exception('collection cannot be empty!')

        if not self.is_connected(): self.connect(self.__url)

        try:
            user = self.__database[col].find_one(filter)
            return user
        except (AttributeError, errors.OperationFailure) as err:
            print('Error during find operation')


    def connect(self, url):
        try:
            self.__database = self.__client[Settings.DB_NAME]
        except errors.ConnectionFailure as err:
            print('Error connection to mongodb: {0}'.format(err))


    def is_connected(self):
        return self.__database
