from src.services.mongo_service import MongoService


class UserService:

    def __init__(self):
        self._mongo_service = MongoService()


    def save(self, user):
        obj = self.convert_to_mongo_object(user)
        result = self._mongo_service.save('users', obj)
        return result


    def activate(self, email):
        result = self._mongo_service.update('users', 'is_active', True, 'email', email)
        return result

    def convert_to_mongo_object(self, user):
        return {
            'email' : user.email,
            'password' : user.password,
            'is_active' : user.is_active
        }
