import hashlib


class Crypto:

    @staticmethod
    def encrypt(password):
        if not password:
            raise Exception('Password cannot be null')

        return(hashlib.md5(password.encode()).hexdigest())
