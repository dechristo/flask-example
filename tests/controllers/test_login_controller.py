import unittest
from unittest.mock import patch
from src.controllers.login_controller import LoginController
from src.models.login import Login

class LoginControllerTest(unittest.TestCase):

    def setUp(self):
        self.mock_params = {
            'email' : 'plagueis@sithlords.com',
            'password' : 'kibercrystal'
        }


    def test_constructor_returns_instance_of_login_controller(self):
        controller = LoginController(self.mock_params)

        self.assertIsInstance(controller, LoginController)


    def test_retuns_none_for_invalid_email(self):
        self.mock_params['email'] = ''
        login_controller = LoginController(self.mock_params)
        result = login_controller.do_login()

        self.assertIsNone(result)


    def test_retuns_none_for_invalid_password(self):
        self.mock_params['email'] = 'plagueis@sithlords.com'
        self.mock_params['password'] = ''
        login_controller = LoginController(self.mock_params)
        result = login_controller.do_login()

        self.assertIsNone(result)


    @patch('src.services.login_service.LoginService.login')
    def test_returns_success_msg_if_user_logged_in(self, login_mock):
        login_mock.return_value = "User {0} logged in!".format('plagueis')
        login_controller = LoginController(self.mock_params)
        result = login_controller.do_login()

        self.assertIsNotNone(result)
        self.assertEqual(result, 'User plagueis@sithlords.com logged in!')
        self.assertTrue(login_mock.called)


    @patch('src.services.login_service.LoginService.login', return_value=None)
    def test_returns_none_if_log_in_failed(self, login_mock):
        self.mock_params['email'] = 'hacked'
        login_controller = LoginController(self.mock_params)
        result = login_controller.do_login()

        self.assertIsNone(result)
        self.assertTrue(login_mock.called)
