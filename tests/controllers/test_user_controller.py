import unittest
from unittest.mock import patch
from src.controllers.user_controller import UserController
from src.models.user import User


class UserControllerTest(unittest.TestCase):

    def setUp(self):
        self.mock_params = {
            'email' : 'plagueis@sithlords.com',
            'password' : 'kibercrystal'
        }


    def test_constructor_returns_instance_of_user_controller(self):
        controller = UserController()

        self.assertIsInstance(controller, UserController)


    @patch('src.services.user_service.UserService.save', return_value=True)
    def test_successfully_created_account_returns_msg_with_activation_link(self, save_mock):
        user_controller = UserController()
        result = user_controller.create_account(self.mock_params)
        expected = 'Your account has been created! Please, activate your account on the following link:' \
                   'http://localhost:5000/api/activate/account'

        self.assertEqual(result, expected)
        self.assertTrue(save_mock.called)


    @patch('src.services.user_service.UserService.save')
    def test_returns_fail_msg_for_invalid_email_param(self, save_mock):
        save_mock.return_value = 'Sorry, cannot create an account without user and password!'
        user_controller = UserController()
        self.mock_params['email'] = ''
        result = user_controller.create_account(self.mock_params)

        self.assertEqual(result, 'Sorry, cannot create an account without user and password!')
        self.assertFalse(save_mock.called)


    @patch('src.services.user_service.UserService.save')
    def test_returns_fail_msg_for_invalid_password_param(self, save_mock):
        save_mock.return_value = 'Sorry, cannot create an account without user and password!'
        user_controller = UserController()
        self.mock_params['email'] = 'plagueis@sithlords.com'
        self.mock_params['password'] = ''
        result = user_controller.create_account(self.mock_params)

        self.assertEqual(result, 'Sorry, cannot create an account without user and password!')
        self.assertFalse(save_mock.called)


    @patch('src.services.user_service.UserService.activate', return_value=False)
    def test_activate_account_msg_for_failed_activation(self, activate_mock):
        user_controller = UserController()
        result = user_controller.activate_account(self.mock_params)

        self.assertEqual(result, 'Uh oh! Something went wrong with the activation.')
        self.assertTrue(activate_mock.called)