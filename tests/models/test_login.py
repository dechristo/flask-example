import unittest
from src.models.login import Login


class LoginTest(unittest.TestCase):

    def setUp(self):
        self.params_mock = {
            'email' : 'test@flask-api.com',
            'password' : 'averyinsecureone'
        }


    def test_constructor_returns_object_with_correct_values(self):
        login = Login(self.params_mock)

        self.assertEqual(login.email, 'test@flask-api.com')
        self.assertEqual(login.password, '6acf9888f53b8333b5d343f2e1680a1a')


    def test_email_property_sets_and_gets_correct_value(self):
        login = Login(self.params_mock)
        login.email = 'test2@flask-api.com'

        self.assertEqual(login.email, 'test2@flask-api.com')
