import unittest
from src.models.user import  User


class UserTest(unittest.TestCase):

    def test_constructor_returns_object_with_correct_values_with_default_logged_and_verified_params(self):
        user = User('test@flask-api.com', 'averyinsecureone')

        self.assertEqual(user.email, 'test@flask-api.com')
        self.assertEqual(user.password, '6acf9888f53b8333b5d343f2e1680a1a')
        self.assertFalse(user.is_logged_in)
        self.assertFalse(user.is_active)


    def test_constructor_returns_object_with_correct_values_with_default_logged_param(self):
        user = User('test@flask-api.com', 'averyinsecureone', True)

        self.assertEqual(user.email, 'test@flask-api.com')
        self.assertEqual(user.password, '6acf9888f53b8333b5d343f2e1680a1a')
        self.assertTrue(user.is_logged_in)
        self.assertEqual(user.is_active, False)


    def test_constructor_returns_object_with_correct_values_with_default_active_param(self):
        user = User('test@flask-api.com', 'averyinsecureone', is_active=True)

        self.assertEqual(user.email, 'test@flask-api.com')
        self.assertEqual(user.password, '6acf9888f53b8333b5d343f2e1680a1a')
        self.assertFalse(user.is_logged_in)
        self.assertTrue(user.is_active)


    def test_set_email_sets_correct_value(self):
        user = User('test@flask-api.com', 'averyinsecureone')
        user.email = 'lukeskywalker@greyjedis.com'
        self.assertEqual(user.email, 'lukeskywalker@greyjedis.com')


    def test_set_is_active_returns_true_if_active(self):
        user = User('test@flask-api.com', 'averyinsecureone')
        user.is_active = True
        self.assertTrue(user.is_active)


    def test_set_is_logged_in_returns_true_if_logged(self):
        user = User('test@flask-api.com', 'averyinsecureone')
        user.is_logged_in = True
        self.assertTrue(user.is_logged_in)
