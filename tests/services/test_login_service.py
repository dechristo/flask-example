import unittest
from unittest.mock import patch
from src.services.login_service import LoginService


class LoginServiceTest(unittest.TestCase):

    def setUp(self):
        self.login_service = LoginService()
        self.user_mock = {
            'email': 'luke@skywalker.com',
            'password': 'usingtheforce',
            'is_active': True
        }

        self.login_info_mock = {
            'email': 'luke@skywalker.com',
            'password': 'usingtheforce'
        }


    def test_constructor_returns_instance_of_login_service(self):
        service = LoginService()

        self.assertIsInstance(service, LoginService)


    @patch('src.services.login_service.LoginService.login')
    def test_returns_user_if_found_and_credential_are_valid(self, login_mock):
        login_mock.return_value = self.user_mock

        result = self.login_service.login(self.login_info_mock)

        self.assertIsNotNone(result)
        self.assertEqual(result, self.user_mock)
        login_mock.assert_called_once_with(self.login_info_mock)


    @patch('src.services.login_service.LoginService.login', return_value=None)
    def test_returns_None_for_invalid_credentials(self, login_mock):
        self.login_info_mock['password'] = 'someinsecon'
        result = self.login_service.login(self.login_info_mock)

        self.assertIsNone(result)
        login_mock.assert_called_once_with(self.login_info_mock)
