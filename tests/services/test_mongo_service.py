import unittest
from unittest.mock import patch

from src.services.mongo_service import MongoService


class MongoServiceTest(unittest.TestCase):

    def setUp(self):
        self._mongo_service = MongoService()
        self.user_mock =   {
            'email' : 'snoke@firstorder.com',
            'password' : 'someinsecureone',
            'is_active' : False
        }

        self.filter_mock = {
            '$and': [
                {'email': 'snoke@firstorder.com'},
                {'password': 'someinsecureone'}
            ]}


    def test_constructor_returns_instance_of_mongo_service(self):
        service = MongoService()

        self.assertIsInstance(service, MongoService)


    @patch('src.services.mongo_service.MongoService.save', return_value='User successfully saved!')
    def test_save_returns_msg_for_successfully_saved_object(self, mock_client):
        result = self._mongo_service.save('users', self.user_mock)

        self.assertEqual(result, 'User successfully saved!')
        mock_client.assert_called_once_with('users', self.user_mock)


    @patch('src.services.mongo_service.MongoService.find')
    def test_find_returns_user_if_found_on_db(self, find_mock):
        find_mock.return_value = self.user_mock
        result = self._mongo_service.find('users', self.filter_mock)

        self.assertIsNotNone(result)
        self.assertEqual(result, self.user_mock)
        find_mock.assert_called_once_with('users', self.filter_mock)


    @patch('src.services.mongo_service.MongoService.find')
    def test_find_returns_None_if_no_user_found_on_db(self, find_mock):
        find_mock.return_value = None
        result = self._mongo_service.find('users', 'some invalid filter')

        self.assertIsNone(result)
        find_mock.assert_called_once_with('users', 'some invalid filter')


    def test_find_raises_exception_if_collection_is_empty(self):
        self.assertRaises(Exception, self._mongo_service.find, '', self.filter_mock)


    def test_save_raises_exception_if_collection_is_empty(self):
        self.assertRaises(Exception, self._mongo_service.save, '', self.user_mock)


    def test_save_raises_exception_if_object_is_none(self):
        self.assertRaises(Exception, self._mongo_service.save, 'users', None)


    def test_update_raises_exception_if_collection_is_empty(self):
        self.assertRaises(Exception, self._mongo_service.update, '', 'is_active', True, 'email', self.user_mock['email'])


    def test_update_raises_exception_if_property_is_empty(self):
        self.assertRaises(Exception, self._mongo_service.update, 'uses', '', True, 'email', self.user_mock['email'])


    def test_update_raises_exception_if_filter_is_empty(self):
        self.assertRaises(Exception, self._mongo_service.update, 'users', 'is_active', True, '', self.user_mock['email'])
