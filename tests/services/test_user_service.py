import unittest
from unittest.mock import patch
from src.models.user import User
from src.services.user_service import UserService
from src.utils.crypto import Crypto


class UserServiceTest(unittest.TestCase):

    def setUp(self):
        self._user_service = UserService()
        self.user_mock = User('luke@thegreys.com',Crypto.encrypt('1234acbd'))


    def test_constructor_returns_instance_of_user_service(self):
        service = UserService()

        self.assertIsInstance(service, UserService)


    @patch('src.services.mongo_service.MongoService.save', return_value='User successfully saved!')
    def test_save_returns_msg_for_successfully_saved_object(self, mock_client):
        result = self._user_service.save(self.user_mock)
        user_object = self._user_service.convert_to_mongo_object(self.user_mock)

        self.assertEqual(result, 'User successfully saved!')
        mock_client.assert_called_once_with('users',  user_object)


    @patch('src.services.mongo_service.MongoService.update', return_value='User successfully activated!')
    def test_save_returns_msg_for_successfully_activated_user(self, mock_client):
        result = self._user_service.activate(self.user_mock.email)

        self.assertEqual(result, 'User successfully activated!')
        mock_client.assert_called_once_with('users', 'is_active', True, 'email', self.user_mock.email)


    def test_convert_object_to_mongo_format_object(self):
        user = User('supremeleader@firstorder.com', Crypto.encrypt('embracethelightside'))
        user_obj = self._user_service.convert_to_mongo_object(user)

        self.assertIn('email', user_obj)
        self.assertIn('password', user_obj)
        self.assertIn('is_active', user_obj)
        self.assertEqual(user_obj['email'], 'supremeleader@firstorder.com')
        self.assertEqual(user_obj['password'], '96d864227f334eeb35c9da204f87b553')
        self.assertFalse(user_obj['is_active'])
