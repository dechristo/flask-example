import unittest
from unittest.mock import patch
import requests
import requests_mock


class RouterTest(unittest.TestCase):

    @requests_mock.Mocker()
    def test_route_login_returns_http_200_and_content_for_success_login(self, req_mock):
        req_mock.post(
            'http://localhost:5000/api/login',
            status_code=200,
            json="User {0} logged in!".format('vader@empire.com')
        )

        response = requests.post('http://localhost:5000/api/login', data={
            'email' : 'vader@empire.com',
            'password' : '1234'
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.text,('"User {0} logged in!"'.format('vader@empire.com')))