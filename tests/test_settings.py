import unittest

from src.settings import Settings


class SettingsTest(unittest.TestCase):

    def test_db_url_has_the_correct_value(self):
        self.assertEqual(Settings.DB_URL, 'mongodb://172.19.0.2:27017')


    def test_db_name_has_the_correct_value(self):
        self.assertEqual(Settings.DB_NAME, 'flask_email_auth')