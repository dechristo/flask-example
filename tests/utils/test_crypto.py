import unittest
from src.utils.crypto import Crypto


class CryptoTest(unittest.TestCase):

    def test_raises_exception_if_password_is_empty(self):
        self.assertRaises(Exception, Crypto.encrypt,'')


    def test_encrypt_returns_correct_md5_hex_string(self):
        weak_pwd = '1234'
        not_that_secure = Crypto.encrypt(weak_pwd)

        self.assertEqual(not_that_secure, '81dc9bdb52d04dc20036dbd8313ed055')


    def test_encrypt_returns_correct_md5_hex_string_2(self):
        weak_pwd = 'ALittleLongerPassword77!'
        not_that_secure_still = Crypto.encrypt(weak_pwd)

        self.assertEqual(not_that_secure_still, 'c0a2e1f256ce41a221a1cefc109b45fc')
